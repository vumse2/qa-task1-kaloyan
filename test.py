import os
import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class PythonOrgSearch(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Safari()

    def test_search_in_python_org(self):
        driver = self.driver
        driver.get('https://google.com/')
        elem = driver.find_element_by_id('lst-ib')
        elem.send_keys('selenium')
        elem.send_keys(Keys.RETURN)
        try:
            link = WebDriverWait(driver, 5).until(
                EC.presence_of_element_located((By.LINK_TEXT, 'Selenium - Web Browser Automation'))
            )
        finally:
            self.assertTrue(link.is_displayed)
            if(os.path.exists('./shots') == False):
                os.mkdir('./shots')
            driver.save_screenshot('./shots/test.png')


    def tearDown(self):
        self.driver.close()

if __name__ == "__main__":
    unittest.main()
